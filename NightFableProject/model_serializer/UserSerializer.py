from rest_framework_mongoengine.serializers import DocumentSerializer
from regme.documents import User as RegUser


class UserSerializer(DocumentSerializer):

    class Meta:
        model = RegUser
        depth = 2
        fields = ('id', 'username', 'first_name', 'last_name', 'email', 'is_staff', 'is_active', 'is_superuser', 'last_login', 'date_joined', 'user_permissions')