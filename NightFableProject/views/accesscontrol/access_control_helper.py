__author__ = 'lethanhdat'
from NightFableProject.models.StoryModelManager import *

access_right_matrix = {"admin": ["admin", "write", "read"], "write": ["write", "read"], "read": ["read"]}


def get_access_rights_to_story(story, user):

    if story is None or user is None:
        return []

    access_matrix = access_right_matrix

    if story.owner == user:
        return access_matrix["admin"]

    if story.privacy == "public":
        if user in story.admin:
            return access_matrix["admin"]
        return access_matrix["write"]

    if user in story.admin:
        return access_matrix["admin"]

    if user in story.writer:
        return access_matrix["write"]

    if user in story.reader:
        return access_matrix["read"]

    if story.privacy == "partlypublic":
        return ["read"]

    return []


def get_access_rights_to_chapter(story_chapter, user):
    [rc, story] = StoryModelManager.get_by_chapter_id(story_chapter.id)

    if rc != "ok":
        return []
    return get_access_rights_to_story(story, user)