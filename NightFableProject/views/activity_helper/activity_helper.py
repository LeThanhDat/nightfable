from NightFableProject.settings import client

from django.core.urlresolvers import reverse

import datetime

#verb must less than 20 chars
#activity list:
#update_partner_list
#add_new_chapter
#follow_book


def users_follow_story_feed(user_list, story_id):
    for user in user_list:
        follower_aggregated_feed = client.feed('aggregated', user.id)
        follower_notification_feed = client.feed('notification', user.id)
        follower_aggregated_feed.follow('book', story_id)
        follower_notification_feed.follow('book', story_id)


def users_id_follow_story_feed(user_id_list, story_id):
    for user_id in user_id_list:
        follower_aggregated_feed = client.feed('aggregated', user_id)
        follower_notification_feed = client.feed('notification', user_id)
        follower_aggregated_feed.follow('book', story_id)
        follower_notification_feed.follow('book', story_id)


def users_unfollow_story_feed(user_list, story_id):
    for user in user_list:
        follower_aggregated_feed = client.feed('aggregated', user.id)
        follower_notification_feed = client.feed('notification', user.id)
        follower_aggregated_feed.unfollow('book', story_id)
        follower_notification_feed.unfollow('book', story_id)


def user_unfollow_author_feed(follower_id, followee_id):
    follower_aggregated_feed = client.feed('aggregated', follower_id)
    follower_aggregated_feed.unfollow('user', followee_id)


def user_follow_author_feed(follower_id, followee_id):
    follower_aggregated_feed = client.feed('aggregated', follower_id)
    follower_aggregated_feed.follow('user', followee_id)


def author_add_new_activity(activity, author_id):
    user_feed = client.feed('user', author_id)
    user_feed.add_activity(activity)


def book_add_new_activity(activity, story_id):
    book_feed = client.feed('book', story_id)
    book_feed.add_activity(activity)


def create_activity(activity_actor, activity_verb, activity_object, activity_time=datetime.datetime.now(), activity_foreign_id=None, extra_data={}):
    activity = dict(
        actor=activity_actor,
        verb=activity_verb,
        object=activity_object,
        foreign_id=activity_foreign_id,
        time=activity_time,
        **extra_data
    )
    return activity

