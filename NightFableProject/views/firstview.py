from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, redirect
from django.core.urlresolvers import reverse
from django.template import RequestContext
from operator import attrgetter
import time
import datetime


import cloudinary
import cloudinary.uploader
import cloudinary.api


from NightFableProject.models.StoryModelManager import StoryModelManager
from NightFableProject.models.StoryChapterModelManager import StoryChapterModelManager
from NightFableProject.models.UserManager import UserManager
from NightFableProject.views.viewhelpers.sessionhelper import session_to_message
from NightFableProject.views.viewhelpers.sessionhelper import message_to_session


@login_required
def hello(request):
    return HttpResponse("Congrats, you are logged in, Hello world", {}, context_instance=RequestContext(request))


def homepage(request):
    # send_mail("hi", "hi, are you free this morning", "service.nightfable@gmail.com", ["thanhdat.toby@gmail.com"])
    return render_to_response("themes/triangle/homepage2.html", {}, context_instance=RequestContext(request))


def cloudinary_cors(request):
    return render_to_response("themes/cloudinary/cloudinary_cors.html", {}, context_instance=RequestContext(request))


def story_page(request):
    [success_message, error_message] = session_to_message(request)

    full_story_list = StoryModelManager.get_all()
    storyList = []

    for story in full_story_list:
        description = story.description
        if len(story.description) > 250:
            description = story.description[0:250] + "[...]"
        storyList.append({"id": str(story.id), "name": story.name, "description": description, "image_url": get_cloudinary_image_url(story.cover_cloudinary_id, 261, 269, "fill") })

    return render_to_response("themes/triangle/viewstorylist.html", {"storyList": storyList, "success_message": success_message, "error_message": error_message}, context_instance=RequestContext(request))


def get_cloudinary_image_url(image_id, width=165, height=170, crop="fill"):
    if not image_id:
        return "/static/themes/triangle/images/portfolio/1.jpg"
    return cloudinary.CloudinaryImage(image_id).build_url(width=width, height=height, html_width=width, html_height=height, crop=crop)


@login_required
def story_create_or_edit(request):
    """
    need to split into create and edit
    """
    [success_message, error_message] = session_to_message(request)

    if request.method == "POST":
        return render_to_response("themes/triangle/viewstorylist.html")
    else:
        return render_to_response("themes/triangle/createoreditstory.html", {"success_message": success_message, "error_message": error_message}, context_instance=RequestContext(request))


@login_required
def save_story(request):
    """
    save story first time
    """
    story = StoryModelManager.bulk_set(request.POST)
    StoryModelManager.save(story, request.user)
    return redirect("storypage")


def view_story(request, story_id=None):
    if story_id is None:
        return redirect('storypage')

    [success_message, error_message] = session_to_message(request)
    [info, story] = StoryModelManager.get_by_id(story_id)

    if info != "ok":
        message_to_session(info, story, request)
        return redirect('storypage')

    if not UserManager.has_read_right_to_story(story, request.user):
        message_to_session("error", "You do not have admin right", request)
        return redirect('story_settings', story_id=story_id)
    
    story.image_url = get_cloudinary_image_url(story.cover_cloudinary_id, 850, 400, "fill")
    chapter_list = []
    story_chapter_list = story.chapters
    story_chapter_list.sort(key=attrgetter('title'), reverse=False)
    for chapter in story.chapters:
        chapter_list.append({"title": chapter.title, "id": str(chapter.id)})

    return render_to_response("themes/triangle/viewstory.html", {"story": story, "chapterList": chapter_list, "success_message": success_message, "error_message": error_message}, context_instance=RequestContext(request))


@login_required
def story_settings(request, story_id=None):
    [success_message, error_message] = session_to_message(request)
    [info, story] = StoryModelManager.get_by_id(story_id)
    if info != "ok":
        message_to_session(info, story, request)
        return redirect('story_settings', story_id=story_id)

    if not UserManager.has_admin_right_to_story(story, request.user):
        message_to_session("error", "You do not have admin right", request)
        return redirect('viewstory', story_id=story_id)

    collaborator_list = []

    for user in story.admin:
        collaborator_list.append({"username": user.username, "id": str(user.id), "email": user.email, "access_right": "admin"})
    for user in story.writer:
        collaborator_list.append({"username": user.username, "id": str(user.id), "email": user.email, "access_right": "write"})
    for user in story.reader:
        collaborator_list.append({"username": user.username, "id": str(user.id), "email": user.email, "access_right": "read"})

    cloudinary_upload_json = cloudinary_config(request)

    return render_to_response("themes/triangle/story_settings.html", {"story": story, "collaborator_list": collaborator_list, "success_message": success_message, "error_message": error_message, "cloudinary_upload": cloudinary_upload_json}, context_instance=RequestContext(request))


def cloudinary_config(request):
    return {
        "timestamp":  time.mktime(datetime.datetime.now().timetuple()),
        "callback": request.build_absolute_uri(reverse("cloudinary_cors")),
        "signature": "7ac8c757e940d95f95495aa0f1cba89ef1a8aa7a",
        "api_key": cloudinary._config.api_key
    }


@login_required
def add_story_collaborators(request):
    if request.method == "POST":
        story_id = request.POST.get('story_id')
        admin_list = request.POST.getlist('admin')
        writer_list = request.POST.getlist('write')
        reader_list = request.POST.getlist('read')

        [rc, info] = StoryModelManager.add_collaborators(story_id, admin_list, writer_list, reader_list)
        message_to_session(rc, info, request)

    return redirect('story_settings', story_id=story_id)


@login_required
def write_chapter(request, story_id, chapter_id=None):
    [success_message, error_message] = session_to_message(request)
    [info, story] = StoryModelManager.get_by_id(story_id)

    if info != "ok":
        message_to_session(info, story, request)
        return redirect('storypage')

    [info, story_chapter] = StoryChapterModelManager.get_by_id(chapter_id, True)

    if info != "ok":
        message_to_session(info, story_chapter, request)
        return redirect('viewstory', story_id=story_id)

    return render_to_response("themes/triangle/createoreditchapter.html", {"storychapter": story_chapter, "story": story, "success_message": success_message, "error_message": error_message}, context_instance=RequestContext(request))


def view_chapter(request, story_id, chapter_id=None):
    [success_message, error_message] = session_to_message(request)

    [info, story] = StoryModelManager.get_by_id(story_id)

    if info != "ok":
        message_to_session(info, story, request)
        return redirect('storypage')

    [info, story_chapter] = StoryChapterModelManager.get_by_id(chapter_id)

    if info != "ok":
        message_to_session(info, story_chapter, request)
        return redirect('viewstory', story_id=story_id)

    return render_to_response("themes/triangle/viewchapter.html", {"storychapter": story_chapter, "story": story, "success_message": success_message, "error_message": error_message}, context_instance=RequestContext(request))


@login_required
def chapter_history(request, story_id, chapter_id=None):
    [success_message, error_message] = session_to_message(request)

    [info, story] = StoryModelManager.get_by_id(story_id)

    if info != "ok":
        message_to_session(info, story, request)
        return redirect('storypage')

    [info, story_chapter] = StoryChapterModelManager.get_by_id(chapter_id)

    if info != "ok":
        message_to_session(info, story_chapter, request)
        return redirect('viewstory', story_id=story_id)

    chapter_history = []

    for commit in story_chapter.history:
        chapter_history.append({"contributor_username": commit.contributor.username, "contributor_id": str(commit.contributor.id), "created_date_time": str(commit.created_date_time), "content": commit.content})

    sorted_chaper_history = sorted(chapter_history, key=lambda commit: commit['created_date_time'], reverse=True)

    return render_to_response('themes/triangle/chapter_history.html', { "storychapter": story_chapter, "story": story, "chapter_history": sorted_chaper_history, "success_message": success_message, "error_message": error_message}, context_instance=RequestContext(request))


@login_required
def revert_chapter_from_history(request):
    if request.method == "POST":

        story_id = request.POST['story_id']
        chapter_id = request.POST['chapter_id']
        chapter_content_from_history = request.POST['chapter_content_from_history']
        [info, story_chapter] = StoryChapterModelManager.get_by_id(chapter_id)

        if info != "ok":
            message_to_session(info, story_chapter, request)
            return redirect("chapteredit", story_id=story_id, chapter_id=chapter_id)

        story_chapter.content = chapter_content_from_history
        [info, message] = StoryChapterModelManager.save(story_chapter, request.user)

        if info != "ok":
            message_to_session("error", message, request)
            return redirect("chapteredit", story_id=story_id, chapter_id=chapter_id)

    else:
        message_to_session("error", "Oops, invalid request", request)

    return redirect("chapteredit", story_id=story_id, chapter_id=chapter_id)


@login_required
def save_chapter(request):
    session_to_message(request)

    storychapter = StoryChapterModelManager.bulk_set(request.POST)
    StoryChapterModelManager.save(storychapter, request.user)
    story_id = request.POST["story_id"]

    storychapter_id = request.POST["storychapter_id"]

    if not storychapter_id:
        StoryModelManager.add_chapter(story_id, storychapter)


    return redirect('viewstory', story_id=story_id)


def user_signup(request):
    return render_to_response("themes/triangle/usersignup.html", {}, context_instance=RequestContext(request))


def view_user_profile(request, user_id):
    cloudinary_upload_json = cloudinary_config(request)
    [info, user] = UserManager.get_by_id(user_id)
    if info != "ok":
        message_to_session(info, user, request)
        return redirect("homepage")
    following = ""
    if request.user in user.follower_list:
        following = True
    return render_to_response("themes/triangle/user_profile.html", {"cloudinary_upload_json": cloudinary_upload_json, "user": user, "following": following}, context_instance=RequestContext(request))


def social_page(request, user_id):
    [rc, following_list] = UserManager.get_following_authors(user_id)
    [info, user] = UserManager.get_by_id(user_id)
    if info != "ok":
        message_to_session(info, user, request)
        return redirect("homepage")
    if rc != "ok":
        following_list = []
    follower_list = user.follower_list
    return render_to_response("themes/triangle/social_network.html", {"user": user, "following_list": following_list, "follower_list": follower_list}, context_instance=RequestContext(request))


def vancouver_homepage(request):
    user = request.user
    return render_to_response("themes/vancouver/vacouver-homepage.html", {"user": user}, context_instance=RequestContext(request))
