__author__ = 'myticmoon'
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from NightFableProject.models.UserManager import UserManager
from NightFableProject.models.StoryModelManager import StoryModelManager
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from NightFableProject.model_serializer.UserSerializer import UserSerializer
from django.contrib.auth.decorators import login_required

@login_required
@api_view(['GET', 'POST'])
def getuser_by_name(request):
    if request.method == 'GET':
        username = request.GET.get('username', None)
        if username is None:
            return Response([], status=status.HTTP_400_BAD_REQUEST)
        if is_valid_email(username):
            users = UserManager.get_user_by_email(username)
        else:
            users = UserManager.get_user_by_name(username)
        serializer = UserSerializer(users, many=True)
        return Response({"data": serializer.data}, status=status.HTTP_200_OK)

@api_view(['POST'])
def add_follower_to_user(request):
    idol_id = request.data.get('idol_id', None)
    follower_id = request.data.get('follower_id', None)
    if not idol_id or not follower_id:
        return Response([], status=status.HTTP_400_BAD_REQUEST)
    [rc, info] = UserManager.add_follower(idol_id, follower_id)
    if rc != "ok":
        return Response([], status=status.HTTP_400_BAD_REQUEST)
    return Response({"info": "success"}, status=status.HTTP_200_OK)


@api_view(['POST'])
def remove_follower_from_user(request):
    idol_id = request.data.get('idol_id', None)
    follower_id = request.data.get('follower_id', None)
    if not idol_id or not follower_id:
        return Response([], status=status.HTTP_400_BAD_REQUEST)
    [rc, info] = UserManager.remove_follower(idol_id, follower_id)
    if rc != "ok":
        return Response([], status=status.HTTP_400_BAD_REQUEST)
    return Response({"info": "success"}, status=status.HTTP_200_OK)


@api_view(['POST'])
def add_follower_to_story(request):
    story_id = request.data.get('story_id', None)
    follower_id = request.data.get('follower_id', None)
    if not story_id or not follower_id:
        return Response([], status=status.HTTP_400_BAD_REQUEST)
    [rc, info] = StoryModelManager.add_follower(story_id, follower_id)
    if rc != "ok":
        return Response([], status=status.HTTP_400_BAD_REQUEST)
    return Response({"info": "success"}, status=status.HTTP_200_OK)


@api_view(['POST'])
def remove_follower_from_story(request):
    story_id = request.data.get('story_id', None)
    follower_id = request.data.get('follower_id', None)
    if not story_id or not follower_id:
        return Response([], status=status.HTTP_400_BAD_REQUEST)
    [rc, info] = StoryModelManager.remove_follower(story_id, follower_id)
    if rc != "ok":
        return Response([], status=status.HTTP_400_BAD_REQUEST)
    return Response({"info": "success"}, status=status.HTTP_200_OK)

def is_valid_email(email):
    try:
        validate_email(email)
    except ValidationError as e:
        return False
    return True