__author__ = 'dat'
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from NightFableProject.models.StoryModelManager import StoryModelManager
from NightFableProject.models.UserManager import UserManager

import cloudinary
import cloudinary.uploader

@api_view(['POST'])
def update_story_cover(request):
    if request.method == 'POST':
        try:
            story_id = request.data['story_id']
            image_id = request.data['image_id']
        except Exception as e:
            return Response([], status=status.HTTP_404_NOT_FOUND)

        if story_id is None or image_id is None:
            return Response([], status=status.HTTP_400_BAD_REQUEST)
        [info, message] = StoryModelManager.update_story_cover(story_id, image_id)
        if info != "ok":
            return Response([], status=status.HTTP_400_BAD_REQUEST)
        return Response({"data": []}, status=status.HTTP_200_OK)


@api_view(['POST'])
def update_user_profile_pic(request):
    if request.method == 'POST':
        try:
            user_id = request.data['user_id']
            image_id = request.data['image_id']
        except Exception as e:
            return Response([], status=status.HTTP_404_NOT_FOUND)

        if user_id is None or image_id is None:
            return Response([], status=status.HTTP_400_BAD_REQUEST)
        [info, message] = UserManager.update_user_profile_pic(user_id, image_id)
        if info != "ok":
            return Response([], status=status.HTTP_400_BAD_REQUEST)
        return Response({"data": []}, status=status.HTTP_200_OK)


