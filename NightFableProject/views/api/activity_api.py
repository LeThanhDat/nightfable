from NightFableProject.models.StoryModelManager import StoryModelManager
from NightFableProject.models.UserManager import UserManager
from NightFableProject.templatetags.avatar_helper import *

from django.core.urlresolvers import reverse
from NightFableProject.settings import client

from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view


#main reason to protect Cross Site Request Forging
@api_view(['GET', 'POST'])
def get_aggregated_feed(request):
    user_id = request.data.get('user_id', None)
    last_feed_id = request.data.get('last_feed_id', "")
    activity_feed = get_aggregated_feed_from_client(user_id)
    [rc, extended_activity_list] = supplement_info_to_feed(activity_feed["results"])
    if rc != "ok":
        return Response([], status=status.HTTP_400_BAD_REQUEST)
    if activity_feed["next"] == "":
        next_activity_feed_id = "None"
    else:
        next_activity_feed_id = activity_feed["next"]
    return Response({"extended_activity_list": extended_activity_list, "next": next_activity_feed_id}, status=status.HTTP_200_OK)


@api_view(['GET'])
def get_notification_feed(request):
    user_id = request.data.get('user_id', None)
    last_feed_id = request.data.get('last_feed_id', None)
    activity_feed = get_notification_feed_from_client(user_id, last_feed_id)
    [rc, extended_activity_list] = supplement_info_to_feed(activity_feed)
    if rc != "ok":
        return Response([], status=status.HTTP_400_BAD_REQUEST)
    return Response({"extended_activity_list": extended_activity_list}, status=status.HTTP_200_OK)


def get_aggregated_feed_from_client(user_id, last_feed_id="", limit=20):
    try:
        user_aggregated_feed = client.feed('aggregated', user_id)
        if last_feed_id and last_feed_id != "":
            result = user_aggregated_feed.get(limit=limit, id_lt=last_feed_id)
        else:
            result = user_aggregated_feed.get(limit=limit, offset=0)
        return result
    except Exception as e:
        return []


def get_notification_feed_from_client(user_id, last_feed_id=None, limit=20, **kwargs):
    user_aggregated_feed = client.feed('notification', user_id)
    if last_feed_id:
        result = user_aggregated_feed.get(limit=limit, id_lt=last_feed_id)
    else:
        result = user_aggregated_feed.get(limit=limit, offset=0)
    return result


def supplement_info_to_feed(activity_feed):
    user_id_list = []
    story_id_list = []
    for feed in activity_feed:
        if feed['verb'] == "update_partner_list" or feed['verb'] == "follow_book" or feed['verb'] == "add_new_chapter":
            # actor is user and object is story
            actor_id = feed["activities"][0]["actor"]
            object_id = feed["activities"][0]["object"]
            user_id_list.append(actor_id)
            story_id_list.append(object_id)

    [rc, user_list] = UserManager.get_user_list_by_ids(user_id_list)
    if rc != "ok":
        return ["error", "Can not get activity feed"]
    user_dict = {str(user.id): user for user in user_list}
    [rc, story_list] = StoryModelManager.get_story_list_by_ids(story_id_list)
    if rc != "ok":
        return ["error", "Can not get activity feed"]
    story_dict = {str(story.id): story for story in story_list}

    extended_activity_list = []
    for feed in activity_feed:
        if feed['verb'] == "update_partner_list" or feed['verb'] == "follow_book" and feed['verb'] == "add_new_chapter":
            user = user_dict[feed["activities"][0]["actor"]]
            story = story_dict[feed["activities"][0]["object"]]
            supplement_info = {"user_id": str(user.id), "user_name": user.username, "user_url": reverse('user_profile', kwargs={'user_id': str(user.id)}), "user_avatar_url": avatar_css_md(user),
                               "story_id": str(story.id), "story_title": story.name, "story_url": reverse('viewstory', kwargs={'story_id': str(story.id)})}
            feed = dict(feed, **supplement_info)
        extended_activity_list.append(feed)
    return ["ok", extended_activity_list]