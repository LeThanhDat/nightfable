__author__ = 'myticmoon'
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from NightFableProject.models.StoryModelManager import StoryModelManager

@api_view(['POST'])
def add_follower_to_story(request):
    story_id = request.data.get('story_id', None)
    follower_id = request.data.get('follower_id', None)
    if not story_id or not follower_id:
        return Response([], status=status.HTTP_400_BAD_REQUEST)
    [rc, info] = StoryModelManager.add_follower(story_id, follower_id)
    if rc != "ok":
        return Response([], status=status.HTTP_400_BAD_REQUEST)
    return Response({"info": "success"}, status=status.HTTP_200_OK)


@api_view(['POST'])
def remove_follower_from_story(request):
    story_id = request.data.get('story_id', None)
    follower_id = request.data.get('follower_id', None)
    if not story_id or not follower_id:
        return Response([], status=status.HTTP_400_BAD_REQUEST)
    [rc, info] = StoryModelManager.remove_follower(story_id, follower_id)
    if rc != "ok":
        return Response([], status=status.HTTP_400_BAD_REQUEST)
    return Response({"info": "success"}, status=status.HTTP_200_OK)