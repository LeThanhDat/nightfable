def message_to_session(type, message, request):
    if type == "success":
        request.session["SUCCESS_MESSAGE"] = message
    if type == "error":
        request.session["ERROR_MESSAGE"] = message


def session_to_message(request):
    success_message = None
    error_message = None
    if "SUCCESS_MESSAGE" in request.session:
        success_message = request.session["SUCCESS_MESSAGE"]
        del request.session["SUCCESS_MESSAGE"]
    if "ERROR_MESSAGE" in request.session:
        error_message = request.session["ERROR_MESSAGE"]
        del request.session["ERROR_MESSAGE"]
    return [success_message, error_message]