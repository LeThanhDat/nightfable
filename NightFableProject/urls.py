from django.conf.urls import patterns, include, url
from django.contrib.auth.views import login, logout
from NightFableProject.views.firstview import *
from NightFableProject.views.api.user_api import *
from NightFableProject.views.api.image_api import *
from NightFableProject.views.api.story_api import *
from NightFableProject.views.api.activity_api import *

from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


urlpatterns = patterns('',
    url(r'^$', homepage, name='homepage'),
    url(r'^hello/$', hello),
    url(r'^cloudinary/cloudinary_cors/$', cloudinary_cors, name='cloudinary_cors'),

    url(r'^user/profile/(?P<user_id>\w+)/$', view_user_profile, name='user_profile'),
    url(r'^user/social/(?P<user_id>\w+)/$', social_page, name='user_social'),
    url(r'^user/', include('regme.urls')),

    url(r'^story/$', story_page, name='storypage'),
    url(r'^story/view/$', view_story, name='viewstorynoid'),
    url(r'^story/view/(?P<story_id>\w+)/$', view_story, name='viewstory'),
    url(r'^story/settings/(?P<story_id>\w+)/$', story_settings, name='story_settings'),
    url(r'^story/createoredit/$', story_create_or_edit, name='storycreateoredit'),
    url(r'^story/addcollaborators/$', add_story_collaborators, name='add_collaborator'),
    url(r'^story/savestory/$', save_story, name='savestory'),
    url(r'^story/savestorychapter/$', save_chapter, name='savechapter'),
    url(r'^story/writechapter/(?P<story_id>\w+)/$', write_chapter, name='editchapternoid'),
    url(r'^story/writechapter/(?P<story_id>\w+)/(?P<chapter_id>\w+)/$', write_chapter, name='chapteredit'),
    url(r'^story/viewchapter/(?P<story_id>\w+)/$', view_chapter, name='viewchapternoid'),
    url(r'^story/viewchapter/(?P<story_id>\w+)/(?P<chapter_id>\w+)/$', view_chapter, name='viewchapter'),
    url(r'^story/chapter_history/(?P<story_id>\w+)/$', chapter_history, name='chapter_history_noid'),
    url(r'^story/chapter_history/(?P<story_id>\w+)/(?P<chapter_id>\w+)/$', chapter_history, name='chapter_history'),
    url(r'^story/revert_chapter_from_history/$', revert_chapter_from_history, name='revert_chapter_from_history'),

    url(r'^vancouver/homepage/$', vancouver_homepage, name='vancouver_homepage'),

    url(r'^api/user/getuserbyname/$', getuser_by_name, name='getuser_by_name_api'),
    url(r'^api/user/follow_user/$', add_follower_to_user, name='add_follower_to_user'),
    url(r'^api/user/unfollower_user/$', remove_follower_from_user, name='remove_follower_from_user'),
    url(r'^api/story/updatestorycover/$', update_story_cover, name='update_story_cover_api'),
    url(r'^api/story/updateuserprofilepic/$', update_user_profile_pic, name='update_user_profile_pic_api'),
    url(r'^api/story/follow_book/$', add_follower_to_story, name='add_follower_to_story'),
    url(r'^api/story/unfollow_book/$', remove_follower_from_story, name='remove_follower_from_story'),

    url(r'^api/user/get_aggregated_feed/$', get_aggregated_feed, name='get_aggregated_feed'),
    url(r'^api/user/get_notification_feed/$', get_notification_feed, name='get_notification_feed'),

    url('^accounts/signup/$', user_signup, name='usersignup'),
    url('^accounts/login/$', login, {'template_name': 'auth/login.html'}, name='log-in'),
    url('^accounts/logout/$', logout, {'next_page': '/'}, name='log-out'),


    (r'^' + settings.MEDIA_URL.lstrip('/') + r'(.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT})
) +static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()