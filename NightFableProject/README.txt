To start project
Install Python 3.4
Install pip https://pip.pypa.io/en/latest/installing.html
Install virtualenv - easy_install virtualenv
Install mongoengine: pip install mongoengine
Install restdjangoframework: pip install djangorestframework

I)Set up guide: http://www.jeffknupp.com/blog/2012/02/09/starting-a-django-project-the-right-way/

II)To start the project
/NightFable/NightFable/Scripts/activate
Run server
python manage.py runserver

III)For token login:
http://www.django-rest-framework.org/#requirements

IV)Sample Django MongoDB project
https://github.com/hmarr/django-mumblr

V)Integrate Django and AngularJS
Use verbatim tag
{% verbatim %}
{% endverbatim %}
http://django-angular.readthedocs.org/en/latest/integration.html

VI) Using Django regme for registration
https://github.com/lig/regme
http://blog.yourlabs.org/post/19777151073/how-to-override-a-view-from-an-external-django-app

Notes:

# Integrate Angular JS to Django
# https://thinkster.io/django-angularjs-tutorial/
# http://django-angular.readthedocs.org/en/latest/angular-model-form.html
# cross site request forgery
# http://stackoverflow.com/questions/8078414/a-weird-django-error
# https://docs.djangoproject.com/en/dev/ref/csrf/#how-to-use-it
# https://docs.djangoproject.com/en/1.7/topics/http/shortcuts/
# from django.core.mail import send_mail
# https://github.com/umutbozkurt/django-rest-framework-mongoengine
# is used to make mongoengine work with django-rest-framework

VII) production set up
sudo vim /etc/environment
and set the LC_ALL to something like en_US.UTF-8,mine is:

LANG="en_US.UTF-8"
LC_MESSAGES="C"
LC_ALL="en_US.UTF-8"


install with pip3
DEBUG=0 authbind gunicorn -b 0.0.0.0:80 NightFableProject.wsgi
#the command below to grant admin right for authbind
sudo touch /etc/authbind/byport/80 ; sudo chmod 777 /etc/authbind/byport/80