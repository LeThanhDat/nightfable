__author__ = 'myticmoon'
from django.template import Library
import cloudinary

register = Library()


def avatar_css_lg(user):
    if user.profile_pic_cloudinary_id:
        image_url = cloudinary.CloudinaryImage(user.profile_pic_cloudinary_id).build_url(width=165, height=170, html_width=165, html_height=170, crop="fill")
        return '<img src="'+image_url+'" class="img-responsive" alt="">'
    else:
        username = user.username
        initital = username[0:1]
        return '<span id="profile_pic" class="avatar avatar-plain avatar-lg  avatar-color-146 avatar-letter-'+initital+' img-responsive"></span>'

register.filter('avatar_css_lg', avatar_css_lg)
avatar_css_lg.is_safe = True


def avatar_css_md(user):
    if user.profile_pic_cloudinary_id:
        image_url = cloudinary.CloudinaryImage(user.profile_pic_cloudinary_id).build_url(width=90, height=90, html_width=90, html_height=90, crop="fill")
        return '<img src="'+image_url+'" class="img-responsive" alt="">'
    else:
        username = user.username
        initital = username[0:1]
        return '<span id="profile_pic" class="avatar avatar-plain avatar-md  avatar-color-146 avatar-letter-'+initital+' img-responsive"></span>'

register.filter('avatar_css_md', avatar_css_md)
avatar_css_md.is_safe = True


def avatar_css_sm(user):
    if user.profile_pic_cloudinary_id:
        image_url = cloudinary.CloudinaryImage(user.profile_pic_cloudinary_id).build_url(width=60, height=70, html_width=60, html_height=60, crop="fill")
        return '<img src="'+image_url+'" class="img-responsive" alt="">'
    else:
        username = user.username
        initital = username[0:1]
        return '<span id="profile_pic" class="avatar avatar-plain avatar-sm  avatar-color-146 avatar-letter-'+initital+' img-responsive"></span>'

register.filter('avatar_css_sm', avatar_css_sm)
avatar_css_md.is_safe = True