"""
Django settings for NightFableProject project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import cloudinary
import stream

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

#project path may be for local settings
PROJECT_PATH = os.path.abspath(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '81-bj8$w*l0u-3ettnuk#3pi1-$-(p8p+_hsxq4s-l-pe*a*2#'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

TEMPLATE_DIRS = (
    os.path.join(PROJECT_PATH, 'templates'),
)

if DEBUG:
    TEMPLATE_LOADERS = [
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    ]
else:
    TEMPLATE_LOADERS = [
        ('django.template.loaders.cached.Loader', (
            'django.template.loaders.filesystem.Loader',
            'django.template.loaders.app_directories.Loader',
            'forum.modules.template_loader.module_templates_loader',
            'forum.skins.load_template_source',
        )),
    ]

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.media',
    'django.core.context_processors.csrf',
    'django.core.context_processors.request'
)
ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'NightFableProject',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'mongoengine.django.mongo_auth',
    'regme',
    'rest_framework_mongoengine',
    'cloudinary'
)

ACCOUNT_ACTIVATION_DAYS = 7  # One-week activation window; you may, of course, use a different value.
REGISTRATION_AUTO_LOGIN = True  # Automatically log the user in.
LOGIN_REDIRECT_URL = "/"
SITE = {'domain': 'nightfable.cloudcontrolled.com', 'name': 'Night Fable'}

STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
)

ROOT_URLCONF = 'NightFableProject.urls'

WSGI_APPLICATION = 'NightFableProject.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.dummy'
    }
}

# ENABLE MONGODB
from mongoengine import connect
connect('nightfable', host='mongodb://admin:admin@ds041561.mongolab.com:41561/nightfable')

#USER MONGOENGINE DEFAULT LOGIN
AUTHENTICATION_BACKENDS = (
    'mongoengine.django.auth.MongoEngineBackend',
)

AUTH_USER_MODEL = 'mongo_auth.MongoUser'

# MONGOENGINE_USER_DOCUMENT = 'regme.documents.User'
MONGOENGINE_USER_DOCUMENT = 'NightFableProject.models.User.User'

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'service.nightfable@gmail.com'
EMAIL_HOST_PASSWORD = 'vnpFn0G2GPKy'
EMAIL_PORT = 587

#ENABLE REST FRAMEWORK
REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        # 'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ]
}

#MONGO TO HANDLE SESSION
SESSION_ENGINE = 'mongoengine.django.sessions'
SESSION_SERIALIZER = 'mongoengine.django.sessions.BSONSerializer'

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/
MEDIA_ROOT = os.path.join(PROJECT_PATH, 'static/media')
#static_root is for production environment
STATIC_ROOT = (os.path.join(PROJECT_PATH, 'static/'))
MEDIA_URL = '/static/media/'
STATIC_URL = '/static/'
SITE_ROOT = os.path.dirname(os.path.realpath(__file__))

# STATICFILES_DIRS = (
#     os.path.join(SITE_ROOT, 'static/'),
# )

# Cloudinary config
cloudinary.config(
  cloud_name="nightfable",
  api_key="824371784427314",
  api_secret="gPiI591GT5YI2qAZLPJM6y5OQf8"
)

client = stream.connect('95z4kz52cyu5', 'axzd3xs6y8ngnbzmknqtqsardqzh72bdkm8u2yn3u864zu53uqszer5dvkmw37kg')