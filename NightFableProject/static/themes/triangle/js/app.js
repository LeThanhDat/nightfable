/**
 * Created by myticmoon on 22/7/15.
 */

var get_cloudinary_image_url = function (image_id, width, height)
{
    width = typeof width !== 'undefined' ? width : 167;
    height = typeof height !== 'undefined' ? height : 170;
    if (typeof image_id == 'undefined' || image_id == "None" ) return "/static/themes/triangle/images/portfolio/1.jpg";

    return $.cloudinary.image(image_id, { secure: true, width: width, height: height, crop: 'fill' })[0].src;
};

var get_cloudinary_profile_pic = function (image_id, width, height)
{
    width = typeof width !== 'undefined' ? width : 167;
    height = typeof height !== 'undefined' ? height : 170;
    if (typeof image_id == 'undefined' || image_id == "None" ) return "";

    return $.cloudinary.image(image_id, { secure: true, width: width, height: height, crop: 'fill' })[0].src;
};