from mongoengine import *
from mongoengine.django.auth import User


class ChapterCommit(EmbeddedDocument):
    contributor = ReferenceField(User, required=True, dbref=False)
    created_date_time = DateTimeField(required=True)
    content = StringField(required=True)


class Comment(EmbeddedDocument):
    content = StringField(required=True)
    owner = StringField(required=True)
    created_date_time = DateTimeField(required=True)


class StoryChapterModel(Document):
    title = StringField()
    description = StringField()
    content = StringField(required=True)
    owner = ReferenceField(User, dbref=False)
    history = SortedListField(EmbeddedDocumentField(ChapterCommit), ordering="created_date_time", reverse=False)
    comments = ListField(EmbeddedDocumentField(Comment))
    active = BooleanField(required=True, default=True)
    published = BooleanField(required=True, default=True)