
from mongoengine import *
from NightFableProject.models.StoryChapterModel import *
from mongoengine.django.auth import User

import datetime

from django.template.defaultfilters import slugify


class MemberList(EmbeddedDocument):
    admin = ListField(ReferenceField(User, dbref=False))
    writer = ListField(ReferenceField(User, dbref=False))
    read = ListField(ReferenceField(User, dbref=False))


class Comment(EmbeddedDocument):
    content = StringField(required=True)
    owner = ReferenceField(User, required=True, dbref=False)
    created_date_time = DateTimeField(required=True)


class Rating(EmbeddedDocument):
    rateList = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
    rate = IntField(required=True, choices=rateList)
    owner = ReferenceField(User, required=True, dbref=False)


class Cast(EmbeddedDocument):
    character = StringField(required=True)
    note = StringField()


class StoryModel(Document):
    name = StringField(required=True)
    description = StringField()
    genreList = ("action", "adventure", "chicklit", "fanfiction", "generalfiction", "historicalfinction", "horror", "humor", "mystery/thriller", "non-fiction", "paranormal", "poetry", "random", "romance", "science fiction", "short story", "spiritual", "teen fiction", "vampire", "werewolf")
    genre = StringField(required=True, choices=genreList)
    owner = ReferenceField(User, required=False, dbref=False) #way easier to manage object relation
    admin = ListField(ReferenceField(User, dbref=False))  # will use an ObjectId
    writer = ListField(ReferenceField(User, dbref=False))
    reader = ListField(ReferenceField(User, dbref=False))
    member_list = EmbeddedDocumentField(MemberList)
    privacyList = ("public", "private", "partlypublic")
    privacy = StringField(required=True, choices=privacyList)
    chapters = ListField(ReferenceField(StoryChapterModel, dbref=False))
    comments = ListField(EmbeddedDocumentField(Comment))
    active = BooleanField(required=True, default=True)
    published = BooleanField(required=True, default=True)
    language = StringField(required=True)
    cast = ListField(EmbeddedDocumentField(Cast))
    cover_cloudinary_id = StringField()
    rating = ListField(EmbeddedDocumentField(Rating))
    follower_list = ListField(ReferenceField("self", dbref=False))

    @property
    def activity_author_feed(self):
        '''
        The name of the feed where the activity will be stored; this is normally
        used by the manager class to determine if the activity should be stored elsewehere than
        settings.USER_FEED
        '''
        return self.owner

    @classmethod
    def activity_related_models(cls):
        '''
        Use this hook to setup related models to load during enrichment.
        It must return None or a list of relationships see Django select_related for reference
        '''
        return None

    @property
    def extra_activity_data(self):
        '''
        Use this hook to store extra data in activities.
        If you need to store references to model instances you should use create_model_refeactivity_rence
        eg:
            @property
            def activity_extra_activity_data(self):
                dict('parent_user'=create_reference(self.parent_user))
        '''
        return {}

    @property
    def activity_actor_attr(self):
        '''
        Returns the model instance field that references the activity actor
        '''
        return None

    @property
    def activity_object_attr(self):
        '''
        Returns the reference to the object of the activity
        '''
        # raise NotImplementedError('%s must implement activity_object_attr property' % self.__class__.__name__)
        return self.chapters

    @property
    def activity_actor_id(self):
        return self.owner.id

    @property
    def activity_actor(self):
        return create_reference(self.owner)

    @property
    def activity_verb(self):
        model_name = slugify(self.__class__.__name__)
        return model_name

    @property
    def activity_object(self):
        return create_reference(self.activity_object_attr)

    @property
    def activity_foreign_id(self):
        return self.activity_object

    @property
    def activity_time(self):
        atime = datetime.datetime.now()
        # if is_aware(self.created_at):
        #     atime = make_naive(atime, pytz.utc)
        return atime

    @property
    def activity_notify(self):
        return None

    def create_activity(self):
        extra_data = self.extra_activity_data
        if not extra_data:
            extra_data = {}
        #list of feed that should as well be notified
        to = self.activity_notify
        if to:
            extra_data['to'] = [f.id for f in to]

        activity = dict(
            actor=self.activity_actor,
            verb=self.activity_verb,
            object=self.activity_object,
            foreign_id=self.activity_foreign_id,
            time=self.activity_time,
            # extra_data
            **extra_data
        )
        return activity

def model_content_type(cls):
    # return '%s.%s' % (cls._meta.app_label, cls._meta.object_name)
    return '%s.%s' % ("NightFableProject", "Story")


def create_reference(reference):
    if isinstance(reference, (Document, )):
        return create_model_reference(reference)
    return reference


def create_model_reference(model_instance):
    '''
    creates a reference to a model instance that can be stored in activities
    # >>> from core.models import Like
    # >>> like = Like.object.get(id=1)
    # >>> create_reference(like)
    # core.Like:1
    '''
    content_type = model_content_type(model_instance.__class__)
    content_id = model_instance.pk
    return '%s:%s' % (content_type, content_id)