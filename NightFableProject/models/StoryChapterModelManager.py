__author__ = 'myticmoon'
from NightFableProject.models.StoryChapterModel import *
import datetime


class StoryChapterModelManager():
    # def __init__(self):
    #     pass

    @staticmethod
    def get_by_id(id, default=False):
        try:
            if id is not None:
                story_chapter = StoryChapterModel.objects(id=id)
                story_chapter = story_chapter[0]

                if not story_chapter:
                    if default:
                        story_chapter = StoryChapterModelManager.make()
                        return ["ok", story_chapter]
                    else:
                        return ["error", "Can not Get Story Chapter With id"]

                return ["ok", story_chapter]

            else:
                if default:
                    story_chapter = StoryChapterModelManager.make()
                    return ["ok", story_chapter]
                else:
                    return ["error", "Can not Get Story Chapter With id"]

        except Exception as e:
            if default:
                story_chapter = StoryChapterModelManager.make()
                return ["ok", story_chapter]
            else:
                return ["error", "Can not Get Story Chapter With id"]

    @staticmethod
    def make():
        story_chapter = StoryChapterModel()
        story_chapter.title = ""
        story_chapter.content = ""
        story_chapter.id = ""
        story_chapter.history = []
        story_chapter.comments = []
        story_chapter.active = True
        story_chapter.published = True
        return story_chapter

    @staticmethod
    def create_story_chapter_model(name, content, story_id):
        # story = StoryModel(name=name, description=description, owner=owner, privacy_level=privacy_level)
        return None

    @staticmethod
    def bulk_set(data, prefix="storychapter_"):
        length = len(prefix)
        if data["storychapter_id"] not in [None, '', 'None']:
            [info, storychapter_object] = StoryChapterModelManager.get_by_id(data["storychapter_id"])
        else:
            storychapter_object = StoryChapterModel()
        for key, item in data.items():
            if item not in [None, ''] and key[:length] == prefix:
                key = key[length:]
                setattr(storychapter_object, key, item)
        return storychapter_object

    @staticmethod
    def save(story_chapter, user):
        try:
            story_chapter.save()

            history = story_chapter.history.sort(key=lambda x: x.created_date_time, reverse=True)

            last_commit = ChapterCommit()

            if history:
                last_commit = history[0]

            if last_commit.content != story_chapter.content:
                new_commit = ChapterCommit()
                new_commit.content = story_chapter.content
                new_commit.contributor = user
                new_commit.created_date_time = datetime.datetime.now()
                story_chapter.update(add_to_set__history=new_commit)

            return ["ok", "Successfully Save Story Chapter"]
        except Exception as e:
            return ["error", "Can not Save Story Chapter"]