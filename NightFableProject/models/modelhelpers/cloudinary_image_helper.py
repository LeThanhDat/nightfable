import cloudinary
import cloudinary.uploader


def remove_photo_from_cloudant(image_id):
        cloudinary.uploader.destroy(image_id, invalidate=True)