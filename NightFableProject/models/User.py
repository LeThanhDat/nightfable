from regme.documents import User as RegUser
from datetime import datetime, timedelta
from hashlib import sha1
from random import random

from django.template.defaultfilters import slugify
from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string
from mongoengine import *

class User(RegUser):

    ''' Implement custom functions for User class here '''
    activation_key = StringField()
    activation_due = DateTimeField()
    profile_pic_cloudinary_id = StringField()
    follower_list = ListField(ReferenceField("self", dbref=False))

    @staticmethod
    def ensure_inactive(klass, document):

        if document.id is None and not document.is_superuser:
            document.deactivate(save=False)

    @staticmethod
    def make_key():
        return sha1((settings.SECRET_KEY + str(random())).encode()).hexdigest()

    def deactivate(self, save=True, notify=True):
        self.is_active = False
        self.activation_key = self.make_key()
        self.activation_due = (
            datetime.utcnow() + timedelta(
                days=settings.ACCOUNT_ACTIVATION_DAYS))

        if save:
            self.save()

        if notify:
            c = {
                'site': settings.SITE,
                'user': self,
            }
            subject = render_to_string(
                'registration/email/activate_subject.txt', c)
            # Email subject *must not* contain newlines
            subject = ''.join(subject.splitlines())
            email = render_to_string(
                'registration/email/activate_email.txt', c)
            send_mail(subject, email, None, [self.email])

        return True

    def activate(self, activation_key, save=True):

        if self.activation_key != activation_key:
            return False
        if self.activation_due < datetime.utcnow():
            return False
        if self.is_active:
            return False

        self.is_active = True
        if save:
            self.save()
        return True

    def has_usable_password(self):
        """Dummy method for django.contrib.auth.forms.PasswordResetForm
        compatibility
        """
        return True

    @property
    def activity_author_feed(self):
        '''
        The name of the feed where the activity will be stored; this is normally
        used by the manager class to determine if the activity should be stored elsewehere than
        settings.USER_FEED
        '''
        return self.owner

    @classmethod
    def activity_related_models(cls):
        '''
        Use this hook to setup related models to load during enrichment.
        It must return None or a list of relationships see Django select_related for reference
        '''
        return None

    @property
    def extra_activity_data(self):
        '''
        Use this hook to store extra data in activities.
        If you need to store references to model instances you should use create_model_refeactivity_rence
        eg:
            @property
            def activity_extra_activity_data(self):
                dict('parent_user'=create_reference(self.parent_user))
        '''
        return {}

    @property
    def activity_actor_attr(self):
        '''
        Returns the model instance field that references the activity actor
        '''
        return None

    @property
    def activity_object_attr(self):
        '''
        Returns the reference to the object of the activity
        '''
        # raise NotImplementedError('%s must implement activity_object_attr property' % self.__class__.__name__)
        return self.chapters

    @property
    def activity_actor_id(self):
        return self.owner.id

    @property
    def activity_actor(self):
        return create_reference(self.owner)

    @property
    def activity_verb(self):
        model_name = slugify(self.__class__.__name__)
        return model_name

    @property
    def activity_object(self):
        return create_reference(self.activity_object_attr)

    @property
    def activity_foreign_id(self):
        return self.activity_object

    @property
    def activity_time(self):
        atime = datetime.datetime.now()
        # if is_aware(self.created_at):
        #     atime = make_naive(atime, pytz.utc)
        return atime

    @property
    def activity_notify(self):
        return None

    def create_activity(self):
        extra_data = self.extra_activity_data
        if not extra_data:
            extra_data = {}
        #list of feed that should as well be notified
        to = self.activity_notify
        if to:
            extra_data['to'] = [f.id for f in to]

        activity = dict(
            actor=self.activity_actor,
            verb=self.activity_verb,
            object=self.activity_object,
            foreign_id=self.activity_foreign_id,
            time=self.activity_time,
            # extra_data
            **extra_data
        )
        return activity

def model_content_type(cls):
    # return '%s.%s' % (cls._meta.app_label, cls._meta.object_name)
    return '%s.%s' % ("NightFableProject", "User")


def create_reference(reference):
    if isinstance(reference, (Document, )):
        return create_model_reference(reference)
    return reference


def create_model_reference(model_instance):
    '''
    creates a reference to a model instance that can be stored in activities
    # >>> from core.models import Like
    # >>> like = Like.object.get(id=1)
    # >>> create_reference(like)
    # core.Like:1
    '''
    content_type = model_content_type(model_instance.__class__)
    content_id = model_instance.pk
    return '%s:%s' % (content_type, content_id)

pre_save.connect(User.ensure_inactive, User)