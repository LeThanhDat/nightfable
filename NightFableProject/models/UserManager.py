from NightFableProject.models.User import User as RegUser
from NightFableProject.views.accesscontrol.access_control_helper import get_access_rights_to_chapter
from NightFableProject.views.accesscontrol.access_control_helper import get_access_rights_to_story
from NightFableProject.models.modelhelpers.cloudinary_image_helper import remove_photo_from_cloudant
from NightFableProject.views.activity_helper.activity_helper import *


class UserManager:

    @staticmethod
    def get_user_by_name(username):
        users = RegUser.objects(username__icontains=username)
        return users

    @staticmethod
    def get_user_by_email(email):
        users = RegUser.objects(email__icontains=email)
        return users

    @staticmethod
    def get_by_username(username):
        try:
            story = RegUser.objects(username=username)
            story = story[0]
            if not story:
                return ["error", "Can not get user with username: " + username]
            return ["ok", story]
        except Exception as e:
            return ["error", "Can not get user with username: " + username]

    @staticmethod
    def get_by_id(id):
        try:
            story = RegUser.objects(id=id)
            story = story[0]
            if not story:
                return ["error", "Can not get user with id: " + id]
            return ["ok", story]
        except Exception as e:
            return ["error", "Can not get user with id: " + id]
        
    @staticmethod
    def get_user_list_by_ids(ids):
        try:
            user_list = RegUser.objects(id__in=ids)
            return ["ok", user_list]
        except Exception as e:
            return ["error", "Can not get user list"]

    @staticmethod
    def has_admin_right_to_story(story, user):
        access_right = get_access_rights_to_story(story, user)
        if "admin" in access_right:
            return True
        return False

    @staticmethod
    def has_write_right_to_story(story, user):
        access_right = get_access_rights_to_story(story, user)
        if "write" in access_right:
            return True
        return False

    @staticmethod
    def has_read_right_to_story(story, user):
        access_right = get_access_rights_to_story(story, user)
        if "read" in access_right:
            return True
        return False

    @staticmethod
    def has_admin_right_to_story_chapter(story_chapter, user):
        access_right = get_access_rights_to_chapter(story_chapter, user)
        if "admin" in access_right:
            return True
        return False

    @staticmethod
    def has_write_right_to_story_chapter(story_chapter, user):
        access_right = get_access_rights_to_chapter(story_chapter, user)
        if "write" in access_right:
            return True
        return False

    @staticmethod
    def has_read_right_to_story_chapter(story_chapter, user):
        access_right = get_access_rights_to_chapter(story_chapter, user)
        if "read" in access_right:
            return True
        return False

    @staticmethod
    def update_user_profile_pic(user_id, image_id):
        [rc, user] = UserManager.get_by_id(user_id)
        if rc != "ok":
            return ["error", "Can Not Update Story Cover"]
        old_profile_pic = user.profile_pic_cloudinary_id
        user.profile_pic_cloudinary_id = image_id
        [info, message] = UserManager.save(user)
        if info != "ok":
            return ["error", "Can Not Update Story Cover"]
        if old_profile_pic:
            remove_photo_from_cloudant(old_profile_pic)
        return ["ok", "Successfully Update Story Cover"]

    @staticmethod
    def add_follower(followee_id, follower_id):
        [rc, idol] = UserManager.get_by_id(followee_id)
        if rc != "ok":
            return ["error", "Can Not Find Idol Id"]
        [rc, follower] = UserManager.get_by_id(follower_id)
        if rc != "ok":
            return ["error", "Can Not Find Follower Id"]
        try:
            idol.update(add_to_set__follower_list=follower_id)
            user_follow_author_feed(follower_id, followee_id)

        except Exception as e:
            return ["error", "Can not add new follower"]
        return ["ok", "Successfully add new follower"]

    @staticmethod
    def remove_follower(followee_id, follower_id):
        [rc, followee] = UserManager.get_by_id(followee_id)
        if rc != "ok":
            return ["error", "Can Not Find Idol Id"]
        [rc, follower] = UserManager.get_by_id(follower_id)
        if rc != "ok":
            return ["error", "Can Not Find Follower Id"]
        follower_list = followee.follower_list
        if follower not in follower_list:
            return ["ok", "Follower Is Not In The List"]
        else:
            follower_list.remove(follower)
        try:
            followee.update(pull_all__follower_list=followee.follower_list)
            followee.update(add_to_set__follower_list=follower_list)
            UserManager.save(followee)
            user_unfollow_author_feed(follower_id, followee_id)

        except Exception as e:
            return ["error", "Can Not Remove Follower"]
        return ["ok", "Successfully Remove Follower"]

    @staticmethod
    def get_following_authors(user_id):
        [rc, user] = UserManager.get_by_id(user_id)
        if rc != "ok":
            return ["error", "Can Not Get Following Authors"]
        try:
            following_authors = RegUser.objects(follower_list__in=[user_id])
        except Exception as e:
            return ["error", "Can Not Get Following Author(s)"]
        return ["ok", following_authors]

    @staticmethod
    def get_following_books(user_id):
        return None

    @staticmethod
    def save(user):
        try:
            user.save()
            return ["ok", "Successfully Save User"]
        except Exception as e:
            return ["error", "Unsuccessfully User"]