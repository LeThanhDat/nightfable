__author__ = 'MyticMoon'
from NightFableProject.models.StoryModel import *
from NightFableProject.models.UserManager import UserManager
from NightFableProject.models.modelhelpers.cloudinary_image_helper import remove_photo_from_cloudant
from NightFableProject.views.activity_helper.activity_helper import *

class StoryModelManager():

    @staticmethod
    def get_by_id(id):
        try:
            story = StoryModel.objects(id=id)
            story = story[0]
            if not story:
                return ["error", "Can not get story with id: " + id]
            return ["ok", story]
        except Exception as e:
            return ["error", "Can not get story with id: " + id]

    @staticmethod
    def get_story_list_by_ids(ids):
        try:
            story_list = StoryModel.objects(id__in=ids)
            return ["ok", story_list]
        except Exception as e:
            return ["error", "Can not get story list with ids"]
        
    @staticmethod
    def get_by_chapter_id(chapter_id):
        try:
            story = StoryModel.objects(chapters__contains=chapter_id)
            story = story[0]
            if not story:
                return ["error", "Can not get story"]
            return ["ok", story]
        except Exception as e:
            return ["error", "Can not get story"]

    @staticmethod
    def get_all():
        try:
            storyList = StoryModel.objects
            return storyList
        except Exception as e:
            return []

    @staticmethod
    def create_story_model(name, description, owner, privacy_level):
        story = StoryModel(name=name, description=description, owner=owner, privacy_level=privacy_level)
        return story


    @staticmethod
    def bulk_set(data, prefix="story_"):
        length = len(prefix)
        if "story_id" in data:
            if data["story_id"] not in [None, '']:
                [info, story_object] = StoryModelManager.get_by_id(data["story_id"])
            else:
                story_object = StoryModel()
        else:
            story_object = StoryModel()
        for key, item in data.items():
            if item not in [None, ''] and key[:length] == prefix:
                key = key[length:]

                setattr(story_object, key, item)

        return story_object


    @staticmethod
    def add_chapter(story_id, storychapter):
        try:
            [info, story] = StoryModelManager.get_by_id(story_id)
            story.chapters.append(storychapter)
            storyChapterList = story.chapters
            story.chapters = list(set(storyChapterList))

            [info, message] = StoryModelManager.save(story)

            if info != "ok":
                return ["error", "Can not Add New Chapter To Story"]

            activity = create_activity(str(story.owner.id), "add_new_chapter", story_id)
            author_add_new_activity(activity, story.owner.id)
            book_add_new_activity(activity, story_id)

            return ["ok", "Successfully Add New Chapter To Story"]
        except Exception as e:
            return ["error", "Can not Add New Chapter To Story"]


    @staticmethod
    def add_collaborators(story_id, admin_list, writer_list, reader_list):
        [rc, story] = StoryModelManager.get_by_id(story_id)
        if rc != "ok":
            return ["error", "Can not add collaborator to story"]

        admin_id_list  = []
        writer_id_list = []
        reader_id_list = []
        seen = set()

        #to remove duplicate in the list, admin > writer > reader
        for story_admin in admin_list:
            if story_admin not in seen and story_admin != '':
                seen.add(story_admin)
                [info, user] = UserManager.get_by_username(story_admin)
                if info == 'ok':
                    admin_id_list.append(str(user.id))

        for story_writer in writer_list:
            if story_writer not in seen and story_writer != '':
                seen.add(story_writer)
                [info, user] = UserManager.get_by_username(story_writer)
                if info == 'ok':
                    writer_id_list.append(str(user.id))

        for story_reader in reader_list:
            if story_reader not in seen and story_reader != '':
                seen.add(story_reader)
                [info, user] = UserManager.get_by_username(story_reader)
                if info == 'ok':
                    reader_id_list.append(str(user.id))

        try:
            new_list = list(set(admin_id_list + writer_id_list + reader_id_list))
            existing_list = list(set(story.admin + story.writer + story.reader))
            existing_list = [str(item.id) for item in existing_list]

            [added_list, removed_list] = StoryModelManager.__compare_list(existing_list, new_list)

            story.update(pull_all__admin=story.admin)
            story.update(pull_all__writer=story.writer)
            story.update(pull_all__reader=story.reader)

            users_unfollow_story_feed(removed_list, story_id)

            story.update(add_to_set__admin=admin_id_list)
            story.update(add_to_set__writer=writer_id_list)
            story.update(add_to_set__reader=reader_id_list)

            users_id_follow_story_feed(added_list, story_id)

        except Exception as e:
            return ["error", "Can not update story's collaborator(s)"]

        activity = create_activity(str(story.owner.id), "update_partner_list", story_id)
        book_add_new_activity(activity, story_id)

        return ["ok", "Successfully update story's collaborator(s)"]

    @staticmethod
    def update_story_cover(story_id, image_id):
        [rc, story] = StoryModelManager.get_by_id(story_id)
        if rc != "ok":
            return ["error", "Can Not Update Story Cover"]
        old_story_cover = story.cover_cloudinary_id
        story.cover_cloudinary_id = image_id
        [info, message] = StoryModelManager.save(story)
        if info != "ok":
            return ["error", "Can Not Update Story Cover"]
        if old_story_cover:
            remove_photo_from_cloudant(old_story_cover)
        return ["ok", "Successfully Update Story Cover"]


    @staticmethod
    def add_follower(story_id, follower_id):
        [rc, story] = StoryModelManager.get_by_id(story_id)
        if rc != "ok":
            return ["error", "Can Not Find Idol Id"]
        [rc, follower] = UserManager.get_by_id(follower_id)
        if rc != "ok":
            return ["error", "Can Not Find Follower Id"]
        try:
            story.update(add_to_set__follower_list=follower_id)
            users_follow_story_feed([follower_id], story_id)

        except Exception as e:
            return ["error", "Can not add new follower"]

        activity = create_activity(follower_id, "follow_book", story_id)
        book_add_new_activity(activity, story_id)
        return ["ok", "Successfully add new follower"]

    @staticmethod
    def remove_follower(story_id, follower_id):
        [rc, story] = StoryModelManager.get_by_id(story_id)
        if rc != "ok":
            return ["error", "Can Not Find Idol Id"]
        [rc, follower] = UserManager.get_by_id(follower_id)
        if rc != "ok":
            return ["error", "Can Not Find Follower Id"]
        follower_list = story.follower_list
        if follower not in follower_list:
            return ["ok", "Follower Is Not In The List"]
        else:
            follower_list.remove(follower)
        try:
            story.update(pull_all__follower_list=story.follower_list)
            story.update(add_to_set__follower_list=follower_list)
            StoryModelManager.save(story)
            users_unfollow_story_feed([follower], story_id)

        except Exception as e:
            return ["error", "Can Not Remove Follower"]
        return ["ok", "Successfully Remove Follower"]

    @staticmethod
    def save(story, user=None):
        try:
            if story.id is None:
                story.owner = user
            story.save()

            return ["ok", "Successfully Save Story"]
        except Exception as e:
            return ["error", "Unsuccessfully Save Story"]

    @staticmethod
    def publish_story(story_id):
        # TODO: implement this function
        return None

    @staticmethod
    def __compare_list(existing_list, new_list):
        added_list = [item for item in new_list if item not in existing_list]
        removed_list = [item for item in existing_list if item not in new_list]
        return [added_list, removed_list]